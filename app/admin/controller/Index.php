<?php

namespace app\admin\controller;

// 引用控制器基类
// use app\admin\controller\Adminbase;
use think\facade\Cache;
use think\facade\view;
use think\facade\Db;
use think\facade\Route;
// use app\admin\controller\AdminBase;

class Index //extends AdminBase
{
    public function index()
    {

        // 设置要给模板赋值的信息
        $list['webtitle'] = '管理员列表';
        $list['dataurl'] = '/admin/index/data';
        $list['status'] = '/admin/index/status';
         // 模板变量赋值
        View::assign('list',$list);
        $sub_menu_list=[
          ['title'=>'首页',
          'url'=>(string) \think\facade\Route::buildUrl('admin/index/welcome',[],'html'),
          'icon'=>'right'],
          ['title'=>'统计页面',
          'url'=>(string) \think\facade\Route::buildUrl('admin/index/welcome1',[],'html'),
          'icon'=>'right']
        ];
        // $list['webtitle'] = '管理员列表';
        // $list['dataurl'] = '/admin/index/data';
        // $list['status'] = '/admin/index/status';
         // 模板变量赋值
        View::assign('sub_menu_list',$sub_menu_list);
 
        // 模板输出
        return View::fetch('index');
        // 模板赋值
        // $this->view->assign('list', $list);

        // // 渲染模板
        // return $this->view->fetch();// return '您好！这是一个[admin]示例应用';
    }
    public function welcome(){
      return View::fetch();  
    }
    public function welcome1(){
       //  $url1=url('admin/index/welcome1', [],'html')->build();
       // $url2= url('index/blog/read', ['id'=>5])
       //  ->suffix('html');
       //  echo ($url2);
       // halt($url1);
      return View::fetch();  
    }
}
