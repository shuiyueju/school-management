<?php

namespace app\admin\controller;

// 引用控制器基类
use app\BaseController;
use think\facade\Cache;
use think\facade\view;
use think\facade\Db;


class public extends BaseController
{
    public function nav_left()
    {

        // 设置要给模板赋值的信息
        $sub_menu_list=[
          ['title'=>'首页','url'=>urlbuild('admin/index/welcome',[],'html'),'icon'=>'&#xe6a7;'],
          ['title'=>'统计页面','url'=>urlbuild('admin/index/welcome1',[],'html'),'icon'=>'&#xe6a7;']
        ];
        // $list['webtitle'] = '管理员列表';
        // $list['dataurl'] = '/admin/index/data';
        // $list['status'] = '/admin/index/status';
         // 模板变量赋值
        View::assign('sub_menu_list',$sub_menu_list);
        // halt(dump($sub_menu_list));
 
        // 模板输出
        return View::fetch();

    }
    public function welcome(){
      return View::fetch();  
    }
    public function welcome1(){       
      return View::fetch();  
    }
}
