<?php

namespace app\admin\controller;

// 引用控制器基类
// use app\admin\controller\Adminbase;
use think\facade\Cache;
use think\facade\view;
use think\facade\Db;
use think\facade\Route;
use think\facade\Request;
use app\Admin\model\Member as MemberModel;
// use app\admin\controller\AdminBase;

class member //extends AdminBase
{
    //读取会员信息
    public function readmember(){
      $id=Request::param('uid');
     // $id=1;
     $model=new MemberModel();

      $res=$model->getMemberById($id);
       // dump($res);
      View::assign('list',$res);
      
      return View::fetch('read');
    }
    //删除指定id的会员
    public function delbyid()
    {
      $id=Request::Instance()->param('id');  
      
       $res=Db::name('member')->where(['id'=>$id])->find();
       $username=$res['realname'];
       $res = Db::name('member')->where(['id'=>$id])->delete();

      if(!$res){
        $res =['code'=>0,'msg'=>'error','status'=>0];
       }else{
        $res=['code'=>200,'msg'=>'删除'.$username.'成功','status'=>200];
       }
       return json($res);
    }
    public function index()
    {

        // 设置要给模板赋值的信息
        $list['webtitle'] = '管理员列表';
        $list['dataurl'] = '/admin/index/data';
        $list['status'] = '/admin/index/status';
         // 模板变量赋值
        View::assign('list',$list);
        $sub_menu_list=[
          ['title'=>'首页',
          'url'=>(string) \think\facade\Route::buildUrl('admin/index/welcome',[],'html'),
          'icon'=>'right'],
          ['title'=>'统计页面',
          'url'=>(string) \think\facade\Route::buildUrl('admin/index/welcome1',[],'html'),
          'icon'=>'right']
        ];

         // 模板变量赋值
        View::assign('sub_menu_list',$sub_menu_list);
 
        // 模板输出
        return View::fetch('index');

    }
    public function list1()
    {
      // db::name('member');
      $map=['status'=>1];
      $data = Db::name('member')->where($map)->field('id,username,realname,sex')->select()->toArray();
      if(!$data){
        $return = json(['code'=>200,'status'=>1,'msg'=>'error']);
       }else{
        $return = json(['code'=>200,'status'=>1,'msg'=>'ok','data'=>$data]);
       } 
       // dump($member);die;
      View::assign('data',$return);
      return View::fetch();
    }
    public function list()
    {
      // db::name('member');
      $map=['status'=>1];
      $data = Db::name('member')->where($map)->field('id,username,realname,sex,mobile')->select()->toArray();

      View::assign('data',$data);
      return View::fetch();
    }
    //查询会员数据
    public function list_member(){
       $page=Request::Instance()->param('page');
       $limit=Request::Instance()->param('limit');

       $map=['status'=>1];
       $data = Db::name('member')->where($map)->field('id,username,realname,sex,mobile,score1 as score')
       ->page($page,$limit)
       ->select();//->toArray()
       $count=Db::name('member')->where($map)->count();
       if(!$data){
        $return =['code'=>404,'msg'=>'error'];
       }else{
        $return=['code'=>0,'msg'=>'ok','count'=>$count,'data'=>$data,'limit'=>$limit];
       }

       return json($return);
    }
    //删除会员数据的恢复
    public function del(){
      $data=Db::name('member')->where(['status'=>-1])->select();
      View::assign('data',$data);
      return View::fetch();  
    }
    public function welcome1(){

      return View::fetch();  
    }
}
