<?php
namespace app\admin\controller;

// 引用控制器基类
// use app\common\controller\BaseController;


/**
 * 控制器基础类
 */
abstract class AdminBase
{
    /**
     * 控制器中间件
     * @var array
     */
    protected $middleware = [
        'online'
        ,'login'
        ,'auth'
    ];
}
