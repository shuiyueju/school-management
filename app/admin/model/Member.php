<?php
namespace app\Admin\model;

use think\Model;

class Member extends Model
{
    protected $readonly = ['realname', 'email'];
    protected $autoWriteTimestamp = true;//开启自动写入时间戳字段
    //根据id获取成员信息
    public function getMemberById($id){
      if(empty($id)){
        return [];
      }
      $result=$this->find($id)->toArray();
      if($result){
        $sexs=['未知','男','女'];
        $result['sex']=$sexs[$result['sex']];
      }      

      return $result;


    }


   public function getStatusAttr($value)
    {
        $status = [-1=>'删除',0=>'禁用',1=>'正常',2=>'待审核'];
        return $status[$value];
    }



}
