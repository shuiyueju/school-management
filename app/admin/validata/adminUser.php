<?php
namespace app\admin\validate;

use think\validate;

class AdminUser extends validate{

    Protected $rule=[
            'username'=>'require',
            'password'=>'require',
            'captcha'=>'require|checkCapcha',
            ];

    Protected $message=[
            'username'=>'用户名必须',
            'password'=>'密码必须',
            ];
    protected function  checkCapcha($value, $rule, $data=[]){
        if(!captcha_check($value){
            return '您输入的密码不正确';
            }
        return true;
        }


}