<?php

namespace app\index\controller;

/*错误时的控制器*/
class Error 
{
    public function __call($name,$arguments)
    {
      $result=[
        'status'=>0,
        'message'=>'找不到该控制器',
        'result'=>null
      ];
      return json($result,400);
    }
    
}
