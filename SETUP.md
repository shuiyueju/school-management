ThinkPHP 6.0
===============

> 运行环境要求PHP7.1+。



## 主要新特性

* 采用`PHP7`强类型（严格模式）
* 支持更多的`PSR`规范
* 原生多应用支持
* 更强大和易用的查询
* 全新的事件系统
* 模型事件和数据库事件统一纳入事件系统
* 模板引擎分离出核心
* 内部功能中间件化
* SESSION/Cookie机制改进
* 对Swoole以及协程支持改进
* 对IDE更加友好
* 统一和精简大量用法

## 安装

composer安装很慢,可以使用以下配置从国内镜像站下载
1.从packagist下载
composer config -g repo.packagist composer https://packagist.phpcomposer.com
2.从阿里云下载
composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/

### 1.安装thinkphp6
~~~
composer create-project topthink/think tp 6.0.*
~~~

如果需要更新框架使用
~~~
composer update topthink/framework
~~~

### 2.安装thinkphp扩展库

安装多应用库扩展
~~~
composer require topthink/think-multi-app
~~~

安装验证码库
~~~
composer require topthink/think-captcha
~~~

安装think助手工具库
~~~
composer require topthink/think-helper
~~~

安装PHPOffice/PhpSpreadsheet 
~~~
composer require phpoffice/phpspreadsheet
~~~

安装PHPOffice/Phpword 
~~~
composer require  phpoffice/phpword
~~~
### 下载layui

将其复制到public/static/layui 目录下面

## 说明文档

[完全开发手册](https://www.kancloud.cn/manual/thinkphp6_0/content)

## 参与开发

请参阅 [ThinkPHP 核心框架包](https://github.com/top-think/framework)。

## 版权信息

ThinkPHP遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2006-2020 by ThinkPHP (http://thinkphp.cn)

All rights reserved。

ThinkPHP® 商标和著作权所有者为上海顶想信息科技有限公司。

更多细节参阅 [LICENSE.txt](LICENSE.txt)
